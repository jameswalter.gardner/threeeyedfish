# threeeyedFish
*Calculating the Quantum Fisher Information for nondegenerate internal squeezing (and the generic three-mode system).*

James Gardner, 2022

Current build found [here](https://git.ligo.org/jameswalter.gardner/threeeyedfish).

---
- Contact james.gardner (at) anu.edu.au for any technical enquiries.
