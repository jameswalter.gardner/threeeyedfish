(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      4609,        149]
NotebookOptionsPosition[      3786,        127]
NotebookOutlinePosition[      4121,        142]
CellTagsIndexPosition[      4078,        139]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"goal", "=", 
   FractionBox[
    RowBox[{"2", "A", " ", "B"}], 
    RowBox[{
     RowBox[{"c", "^", "2"}], " ", "+", 
     RowBox[{"A", "^", "2"}], "+", 
     RowBox[{"B", "^", "2"}]}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"deriv", "=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{"goal", ",", "A"}], "]"}], "//", "FullSimplify"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"sol", "=", 
  RowBox[{"Solve", "[", 
   RowBox[{
    RowBox[{"deriv", "\[Equal]", "0"}], ",", "A"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"result", "=", 
  RowBox[{
   RowBox[{"goal", "/.", "sol"}], "//", "FullSimplify"}]}]}], "Input",
 CellChangeTimes->{{3.8903216377037163`*^9, 3.8903217113308153`*^9}, {
  3.890321747752782*^9, 3.890321749963419*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"436d8d3a-6fde-4686-afdc-332683da18cb"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"A", "\[Rule]", 
     RowBox[{"-", 
      SqrtBox[
       RowBox[{
        SuperscriptBox["B", "2"], "+", 
        SuperscriptBox["c", "2"]}]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"A", "\[Rule]", 
     SqrtBox[
      RowBox[{
       SuperscriptBox["B", "2"], "+", 
       SuperscriptBox["c", "2"]}]]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.89032167292564*^9, 3.890321711727779*^9}, 
   3.8903217509571943`*^9},
 CellLabel->"Out[29]=",ExpressionUUID->"74dcd301-7167-4128-a8c1-7fae6f3aaf50"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", 
    FractionBox["B", 
     SqrtBox[
      RowBox[{
       SuperscriptBox["B", "2"], "+", 
       SuperscriptBox["c", "2"]}]]]}], ",", 
   FractionBox["B", 
    SqrtBox[
     RowBox[{
      SuperscriptBox["B", "2"], "+", 
      SuperscriptBox["c", "2"]}]]]}], "}"}]], "Output",
 CellChangeTimes->{{3.89032167292564*^9, 3.890321711727779*^9}, 
   3.890321750959716*^9},
 CellLabel->"Out[30]=",ExpressionUUID->"df49cc2d-f8cc-44f6-97d5-201a7af0b210"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"sub", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"B", "\[Rule]", 
      RowBox[{"2", "\[Pi]", " ", "3000."}]}], ",", 
     RowBox[{"c", "\[Rule]", 
      RowBox[{"2", "\[Pi]", " ", "42"}]}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(", 
   FractionBox[
    RowBox[{"A", "/.", "sol"}], 
    RowBox[{"\[Pi]", " ", "2."}]], ")"}], "/.", 
  "sub"}], "\[IndentingNewLine]", 
 RowBox[{"result", "/.", "sub"}]}], "Input",
 CellChangeTimes->{{3.890321752148234*^9, 3.890321873197111*^9}},
 CellLabel->"In[54]:=",ExpressionUUID->"4cead633-91f5-47d3-a676-48f9762d66f2"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "3000.293985595412`"}], ",", "3000.293985595412`"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.8903217758936167`*^9, 3.890321873502966*^9}},
 CellLabel->"Out[55]=",ExpressionUUID->"bb1b7d81-75a6-45e3-a1e0-52799cd7e091"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "0.9999020144036475`"}], ",", "0.9999020144036475`"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.8903217758936167`*^9, 3.890321873505144*^9}},
 CellLabel->"Out[56]=",ExpressionUUID->"5dd0df51-6dce-433d-a945-07664b6c3e5c"]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{185, Automatic}, {24, Automatic}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 878, 25, 127, "Input",ExpressionUUID->"436d8d3a-6fde-4686-afdc-332683da18cb"],
Cell[1461, 49, 580, 18, 50, "Output",ExpressionUUID->"74dcd301-7167-4128-a8c1-7fae6f3aaf50"],
Cell[2044, 69, 503, 16, 64, "Output",ExpressionUUID->"df49cc2d-f8cc-44f6-97d5-201a7af0b210"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2584, 90, 626, 18, 108, "Input",ExpressionUUID->"4cead633-91f5-47d3-a676-48f9762d66f2"],
Cell[3213, 110, 276, 6, 35, "Output",ExpressionUUID->"bb1b7d81-75a6-45e3-a1e0-52799cd7e091"],
Cell[3492, 118, 278, 6, 35, "Output",ExpressionUUID->"5dd0df51-6dce-433d-a945-07664b6c3e5c"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

