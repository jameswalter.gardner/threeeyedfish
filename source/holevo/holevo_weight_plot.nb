(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     13008,        320]
NotebookOptionsPosition[     12091,        298]
NotebookOutlinePosition[     12455,        314]
CellTagsIndexPosition[     12412,        311]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{"Clear", "[", "tuviaSemiAnalyticsGeneral\[CapitalSigma]and\[Phi]", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tuviaSemiAnalyticsGeneral\[CapitalSigma]and\[Phi]", "[", 
   RowBox[{
    RowBox[{"\[Mu]_", "?", "NumericQ"}], ",", 
    RowBox[{"w_", "?", "NumericQ"}]}], "]"}], ":=", 
  RowBox[{"NMinimize", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"w", " ", 
      FractionBox["1", 
       SuperscriptBox[
        RowBox[{"Cos", "[", "\[Phi]", "]"}], "2"]]}], "+", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "-", "w"}], ")"}], " ", 
      FractionBox["1", 
       SuperscriptBox[
        RowBox[{"Cos", "[", 
         RowBox[{"\[Phi]", "+", 
          RowBox[{"ArcSin", "[", "\[Mu]", "]"}]}], "]"}], "2"]]}]}], ",", 
    RowBox[{"{", "\[Phi]", "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.8805663430349903`*^9, 3.880566581065037*^9}, {
  3.8805679107335777`*^9, 3.8805679461585283`*^9}, {3.8805680133324623`*^9, 
  3.880568017193928*^9}, {3.880571657870573*^9, 3.880571691696537*^9}, {
  3.8831451378520813`*^9, 3.883145161514738*^9}, {3.883145245735375*^9, 
  3.8831452619911327`*^9}, {3.8831461251617804`*^9, 3.883146146299069*^9}, {
  3.883146409109356*^9, 3.883146416123794*^9}, {3.883146447623426*^9, 
  3.883146481166671*^9}, {3.886790015979032*^9, 3.8867900417182627`*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"b13b3b23-0377-49c3-9a36-088ae93d5103"],

Cell["Plot slice for \[Mu]=1 for simplicity.", "Text",
 CellChangeTimes->{{3.886790131078623*^9, 
  3.8867901399167967`*^9}},ExpressionUUID->"5c0a9b19-e6b7-4a5c-a0fc-\
4530ccccc6df"],

Cell[BoxData[
 RowBox[{"Block", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\[Mu]", "=", "1"}], "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Clear", "[", "HolB", "]"}], ";", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"HolB", "[", 
      RowBox[{"w_", "?", "NumericQ"}], "]"}], ":=", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"tuviaSemiAnalyticsGeneral\[CapitalSigma]and\[Phi]", "[", 
        RowBox[{"\[Mu]", ",", "w"}], "]"}], ")"}], "[", 
      RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
    "\[IndentingNewLine]", 
    RowBox[{"plotPoints", "=", "100"}], ";", "\[IndentingNewLine]", 
    RowBox[{"wList", "=", 
     RowBox[{"Subdivide", "[", 
      RowBox[{"0", ",", "1.", ",", 
       RowBox[{"plotPoints", "+", "1"}]}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"data", "=", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"HolB", "[", 
        RowBox[{"wList", "[", 
         RowBox[{"[", "k", "]"}], "]"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"k", ",", "1", ",", 
         RowBox[{"Length", "[", "wList", "]"}]}], "}"}]}], "]"}]}], ";"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.886789946289009*^9, 3.886790007247863*^9}, {
  3.886790076861014*^9, 3.886790113146772*^9}, {3.886790174422297*^9, 
  3.886790180019672*^9}, {3.886790211979576*^9, 3.886790293905748*^9}, {
  3.886790353353101*^9, 3.886790511204802*^9}, {3.886790565245989*^9, 
  3.886790575015072*^9}, {3.886790607291189*^9, 3.886790625015663*^9}, {
  3.886790784675335*^9, 3.8867907985918293`*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"19f2925d-92e3-42c7-90ba-8b28eec134fd"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"ticks", "=", 
   RowBox[{"Subdivide", "[", 
    RowBox[{"0", ",", "1.", ",", "2"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"plot", "=", 
   RowBox[{"ListPlot", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"wList", ",", "data"}], "}"}], "//", "Transpose"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Joined", "\[Rule]", "True"}], ",", "\[IndentingNewLine]", 
     RowBox[{"Frame", "\[Rule]", "True"}], ",", "\[IndentingNewLine]", 
     RowBox[{"AspectRatio", "\[Rule]", "1"}], ",", 
     RowBox[{"LabelStyle", "\[Rule]", "Black"}], ",", 
     RowBox[{"FrameTicksStyle", "\[Rule]", 
      RowBox[{"Directive", "[", "18", "]"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"Automatic", ",", 
        RowBox[{"{", 
         RowBox[{"1", ",", "Automatic"}], "}"}]}], "}"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"PlotStyle", "\[Rule]", 
      RowBox[{"Directive", "[", 
       RowBox[{
        RowBox[{"Thickness", "[", "0.02", "]"}], ",", "Orange"}], "]"}]}], 
     ",", "\[IndentingNewLine]", 
     RowBox[{"FrameTicks", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          RowBox[{"ticks", "+", "1"}], ",", "None"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"ticks", ",", "None"}], "}"}]}], "}"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"GridLines", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"ticks", ",", 
        RowBox[{"ticks", "+", "1"}]}], "}"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"PlotLabel", "\[Rule]", 
      RowBox[{"Style", "[", 
       RowBox[{"\"\<Joint error of A and B for \[Mu] = 1\>\"", ",", 
        RowBox[{"FontSize", "\[Rule]", "18"}], ",", 
        RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}]}], "]"}]}], 
     ",", "\[IndentingNewLine]", 
     RowBox[{"FrameLabel", "\[Rule]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          RowBox[{"Style", "[", 
           RowBox[{"\"\<Holevo Cram\[EAcute]r-Rao Bound\>\"", ",", 
            RowBox[{"FontSize", "\[Rule]", "18"}], ",", 
            RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}]}], "]"}], 
          ",", "\"\<\>\""}], "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"Style", "[", 
           RowBox[{"\"\<weight, w\>\"", ",", 
            RowBox[{"FontSize", "\[Rule]", "18"}], ",", 
            RowBox[{"FontColor", "\[Rule]", "Orange"}], ",", 
            RowBox[{"FontFamily", "\[Rule]", "\"\<Helvetica\>\""}]}], "]"}], 
          ",", "\"\<\>\""}], "}"}]}], "\[IndentingNewLine]", "}"}]}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Print", "[", "plot", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Export", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"ParentDirectory", "[", 
      RowBox[{"NotebookDirectory", "[", "]"}], "]"}], "<>", 
     "\"\</figures/simplified_weight_plot.pdf\>\""}], ",", 
    "\[IndentingNewLine]", "plot"}], "\[IndentingNewLine]", "]"}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.8867906345022373`*^9, 3.88679077456952*^9}, {
  3.8867908192801733`*^9, 3.886790996334305*^9}, {3.8867910430756273`*^9, 
  3.8867911036856737`*^9}, {3.886791146168713*^9, 3.886791201747527*^9}, {
  3.8867912466488953`*^9, 3.8867913192672443`*^9}, {3.886791745133841*^9, 
  3.886791766243174*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"98a720fc-6189-43bb-9e14-56f26b23052a"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[1, 0.5, 0], PointSize[0.009166666666666668], Thickness[0.02], 
     LineBox[CompressedData["
1:eJxVlQ1QFHUYxglDS4gGIuxSFKa6+LDJIKI5yUfDHMvE0HAShlKKnMDCYWgY
rjMj0XIc4xIxE8NRMTzG0aQLB08JJRg7NFJJQIUDjo/7BOngPO6423b3/7oz
3czNzu7+P97/8/zeZ6Oy89fm+Pv5+X3M/4Xr/3/34OnOznGtKMXjBZ3t4XEO
ut+PEw6TdvXnE3CtCDtYmFsFXaAh5/xnk/S+GjWXW2dqXnNCqdZ9Wf+eBmt+
eK7roXEnjT+N9zd82lugvo8L/FtZ41k40kut6kgXzddieVVCXGa1C4pEYed6
KMJPFpoipmi9BnxQ1rbz9++m0GDdvKeiXwd97eTyR1xTtH4jUpJUCk2GG/PS
Nt7QFDdh4RvWFzPOuWm/S5hZskf5S7AHmaeCDN/6N2NxYsZHz2/y0P5/IHrV
+qflZzyonCXs0ILHlHPuz5jyUD2tUMu2nwnENG4Lw8OuYF5s5bltJdNU35+Y
nX/xqX1N05A1hgTIf9Jj14EvDMXT01RvG5Z8XZS8MdGLDeKAa6iscebm5Xmp
/r/wzstc7sUqL/ib+YOn21E/HvBjYbuXzvM3Xl1wPPaU1wubsNzc6yhLt1Wo
Ynx0vuswyXUjnrU+CG/5KQhSLwxaovTReW8iJswSmH7Eh7jmpGNN1Teh2Lp0
adplH52/A/OPHt6+zOhD27290YvMHTh6+LeUBH+O9PgH4Smf6OMXcMjjqzvy
wi3kR0TFr1JwpM8tqGRb9u1ax2H22+ITaKMzQ0ZzOdKrCysrnnTu/opDbbEg
WBeyawMaNu3nSL9uhPbZQ1Q/c3irZnjZqLsbkatjHjbUc6TnbbiC15jLWjiY
O5KvZOEOLkRvVpbf4EjfO+iq7ey093DY7V+eem3HXXxTHKs+NsKR3j0o3dF6
QjvGIWaRsEIPNMq6crmTI/178a+3IM3t5sAvLkgGy7bQ9S/5OPLDAH1v1qGr
HAdti55XqA9X6Z750yeNL707+WuLvg8TtB7zqx8nab93HZFF3i39Uj3Mv37s
pHqfFQUckM7D/ByQzitOPzsAN+nB/DVKeon2rjPiCdKT+W2U9BbVmTRKfjD/
B1FHfn0o2HVwEEryk/EwiK3kd4LqGb7CIcwhHhgfQzhOvMwQNxhCBPHEeBnG
YuKNF4t3fBixxCPjZ1jiVajeETki8cx4GpF4F2OpeQSvUD8wvkw4T/3yOg9P
do4JqdRPjDeT1G+hPE2HZpkB6kfGn1nq1wGhezRmPEr9zHg0Yy71e128IIBF
ygPGpwVBlBclbwoTLJBTnjBerUimvBFxUVulPGL8WqW8ihLsjbdJecZ4tkl5
Ny62p03KQ8a3HUmUl5cE+YrsUp4y3u2YoLz9XjiubBRZlMeM/1GkUl6L5elG
oaE8Z/0whkbKe3G5rDHpe8D6YwzB9L148B15cP0PgnA/sg==
      "]]}}, {}, {}, {}, {}},
  
  AspectRatio->1,
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 1.0052628999157698`},
  DisplayFunction->Identity,
  Frame->{{True, True}, {True, True}},
  FrameLabel->{{
     FormBox[
      StyleBox[
      "\"Holevo Cram\[EAcute]r-Rao Bound\"", FontSize -> 18, FontFamily -> 
       "Helvetica", StripOnInput -> False], TraditionalForm], 
     FormBox["\"\"", TraditionalForm]}, {
     FormBox[
      StyleBox[
      "\"weight, w\"", FontSize -> 18, FontColor -> RGBColor[1, 0.5, 0], 
       FontFamily -> "Helvetica", StripOnInput -> False], TraditionalForm], 
     FormBox["\"\"", TraditionalForm]}},
  FrameTicks->{{{{1., 
       FormBox["1.`", TraditionalForm]}, {1.5, 
       FormBox["1.5`", TraditionalForm]}, {2., 
       FormBox["2.`", TraditionalForm]}}, None}, {{{0., 
       FormBox["0.`", TraditionalForm]}, {0.5, 
       FormBox["0.5`", TraditionalForm]}, {1., 
       FormBox["1.`", TraditionalForm]}}, None}},
  FrameTicksStyle->Directive[18],
  GridLines->{{0., 0.5, 1.}, {1., 1.5, 2.}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  LabelStyle->GrayLevel[0],
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotLabel->FormBox[
    StyleBox[
    "\"Joint error of A and B for \[Mu] = 1\"", FontSize -> 18, FontFamily -> 
     "Helvetica", StripOnInput -> False], TraditionalForm],
  PlotRange->{{0, 1.}, {1, 1.9999509839962455`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {0, 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Print",
 CellChangeTimes->{{3.886791179573433*^9, 3.886791202403368*^9}, {
   3.8867912578612013`*^9, 3.886791291989771*^9}, {3.886791751549622*^9, 
   3.8867917673566723`*^9}, 3.890331759689979*^9},
 CellLabel->
  "During evaluation of \
In[4]:=",ExpressionUUID->"ac145dfd-9d56-43b4-93fd-4307a38a417f"]
}, Open  ]],

Cell["Save data for Python plotter", "Text",
 CellChangeTimes->{{3.8903317662647247`*^9, 
  3.890331776284224*^9}},ExpressionUUID->"2799ffcf-84df-442c-937a-\
033c63e65f2a"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"csvFormat", "=", 
   RowBox[{"Transpose", "[", 
    RowBox[{"{", 
     RowBox[{"wList", ",", "data"}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Export", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"ParentDirectory", "[", 
      RowBox[{"NotebookDirectory", "[", "]"}], "]"}], "<>", 
     "\"\</csv/fig3.csv\>\""}], ",", "\[IndentingNewLine]", "csvFormat"}], 
   "\[IndentingNewLine]", "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.890331812161294*^9, 3.8903318149132843`*^9}, {
  3.89033184501588*^9, 3.8903318966401873`*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"ad23f370-b731-46ab-8d7a-1210fb3f4932"]
},
WindowSize->{1853, 1025},
WindowMargins->{{0, Automatic}, {0, Automatic}},
Magnification:>1.5 Inherited,
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1411, 31, 123, "Input",ExpressionUUID->"b13b3b23-0377-49c3-9a36-088ae93d5103"],
Cell[1972, 53, 182, 3, 54, "Text",ExpressionUUID->"5c0a9b19-e6b7-4a5c-a0fc-4530ccccc6df"],
Cell[2157, 58, 1665, 38, 286, "Input",ExpressionUUID->"19f2925d-92e3-42c7-90ba-8b28eec134fd"],
Cell[CellGroupData[{
Cell[3847, 100, 3614, 84, 717, "Input",ExpressionUUID->"98a720fc-6189-43bb-9e14-56f26b23052a"],
Cell[7464, 186, 3738, 85, 583, "Print",ExpressionUUID->"ac145dfd-9d56-43b4-93fd-4307a38a417f"]
}, Open  ]],
Cell[11217, 274, 172, 3, 54, "Text",ExpressionUUID->"2799ffcf-84df-442c-937a-033c63e65f2a"],
Cell[11392, 279, 695, 17, 241, "Input",ExpressionUUID->"ad23f370-b731-46ab-8d7a-1210fb3f4932"]
}
]
*)

