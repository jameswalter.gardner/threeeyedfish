(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     37699,       1151]
NotebookOptionsPosition[     34596,       1093]
NotebookOutlinePosition[     34960,       1109]
CellTagsIndexPosition[     34917,       1106]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["\<\
Suggested by Simon H. to look at to check understanding of data processing \
inequality between SNR/RMSE and the CFI.

See notebook. In the linear theory, this suggests that nIS\[CloseCurlyQuote]s \
states are Gaussian with the signal in the mean and the noise in the variance \
s.t. the RMSE = NSR = sqrt CCRB. This means that there\[CloseCurlyQuote]s a \
problem in my understanding: if the CCRB is saturated and some RO scheme must \
make CFI = QFI, then why isn\[CloseCurlyQuote]t the QCRB saturated as well?\
\>", "Text",
 CellFrame->{{0, 0}, {3, 0}},
 CellChangeTimes->{{3.871042041100456*^9, 3.871042071473916*^9}, {
  3.871990256782901*^9, 
  3.871990393429755*^9}},ExpressionUUID->"c30710f4-55d3-4376-acb1-\
16b3f2d1ede2"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"$Assumptions", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"\[Sigma]", "[", "h", "]"}], ">", "0"}], ",", " ", 
      RowBox[{
       RowBox[{"\[Mu]", "[", "h", "]"}], "\[Element]", "Reals"}]}], "}"}]}], 
   ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"Gaussian", " ", "probability", " ", "distribution"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"p", "[", 
   RowBox[{"x_", ",", "h_"}], "]"}], ":=", 
  RowBox[{
   FractionBox["1", 
    RowBox[{
     RowBox[{"\[Sigma]", "[", "h", "]"}], 
     RowBox[{"Sqrt", "[", 
      RowBox[{"2", "\[Pi]"}], "]"}]}]], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", 
       RowBox[{"2", " ", 
        SuperscriptBox[
         RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]}]]}], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"x", "-", 
        RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"]}], 
    "]"}]}]}]}], "Input",
 CellChangeTimes->{{3.871040615347677*^9, 3.871040680942831*^9}, {
  3.871043767099353*^9, 3.87104376742647*^9}, {3.87104459971082*^9, 
  3.871044609306074*^9}},ExpressionUUID->"e1a8ec7b-781e-42c5-8e89-\
382851d8ca88"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"Log", "[", 
    RowBox[{"1", "/", 
     RowBox[{"y", "[", "x", "]"}]}], "]"}], ",", "x"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"Log", "[", 
     RowBox[{"p", "[", 
      RowBox[{"x", ",", "h"}], "]"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"h", ",", "1"}], "}"}]}], "]"}], "//", 
  "FullSimplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"Log", "[", 
     RowBox[{"p", "[", 
      RowBox[{"x", ",", "h"}], "]"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"h", ",", "2"}], "}"}]}], "]"}], "//", 
  "FullSimplify"}]}], "Input",
 CellLabel->"In[3]:=",ExpressionUUID->"86f81243-c648-42ba-b779-74f69b36d527"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{
    SuperscriptBox["y", "\[Prime]",
     MultilineFunction->None], "[", "x", "]"}], 
   RowBox[{"y", "[", "x", "]"}]]}]], "Output",
 CellChangeTimes->{3.871043818091543*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"6864a8e0-44fe-4b16-9a4b-877775715091"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   SuperscriptBox[
    RowBox[{"\[Sigma]", "[", "h", "]"}], "3"]], 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"x", "-", 
       RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], " ", 
     RowBox[{"\[Sigma]", "[", "h", "]"}], " ", 
     RowBox[{
      SuperscriptBox["\[Mu]", "\[Prime]",
       MultilineFunction->None], "[", "h", "]"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"x", "-", 
          RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"], "-", 
       SuperscriptBox[
        RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]}], ")"}], " ", 
     RowBox[{
      SuperscriptBox["\[Sigma]", "\[Prime]",
       MultilineFunction->None], "[", "h", "]"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.8710438181812*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"0f2a57e7-dde7-420a-b496-d5d5641e0e9a"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   SuperscriptBox[
    RowBox[{"\[Sigma]", "[", "h", "]"}], "4"]], 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "3"}], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"x", "-", 
        RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"], " ", 
     SuperscriptBox[
      RowBox[{
       SuperscriptBox["\[Sigma]", "\[Prime]",
        MultilineFunction->None], "[", "h", "]"}], "2"]}], "+", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"\[Sigma]", "[", "h", "]"}], "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox[
         RowBox[{
          SuperscriptBox["\[Mu]", "\[Prime]",
           MultilineFunction->None], "[", "h", "]"}], "2"]}], "+", 
       SuperscriptBox[
        RowBox[{
         SuperscriptBox["\[Sigma]", "\[Prime]",
          MultilineFunction->None], "[", "h", "]"}], "2"], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"x", "-", 
          RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], " ", 
        RowBox[{
         SuperscriptBox["\[Mu]", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "h", "]"}]}]}], ")"}]}], "-", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"\[Sigma]", "[", "h", "]"}], "3"], " ", 
     RowBox[{
      SuperscriptBox["\[Sigma]", "\[Prime]\[Prime]",
       MultilineFunction->None], "[", "h", "]"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"x", "-", 
       RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], " ", 
     RowBox[{"\[Sigma]", "[", "h", "]"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "4"}], " ", 
        RowBox[{
         SuperscriptBox["\[Mu]", "\[Prime]",
          MultilineFunction->None], "[", "h", "]"}], " ", 
        RowBox[{
         SuperscriptBox["\[Sigma]", "\[Prime]",
          MultilineFunction->None], "[", "h", "]"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"x", "-", 
          RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], " ", 
        RowBox[{
         SuperscriptBox["\[Sigma]", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "h", "]"}]}]}], ")"}]}]}], 
   ")"}]}]], "Output",
 CellChangeTimes->{3.871043818349125*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"1898a327-af66-48d4-836e-c3540ce23d13"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"classical", " ", "Fisher", " ", "information"}], ",", " ", 
    RowBox[{"recovers", " ", 
     FractionBox[
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         SuperscriptBox["\[Mu]", "\[Prime]",
          MultilineFunction->None], ")"}], "2"], "+", 
       RowBox[{"2", 
        SuperscriptBox[
         RowBox[{"(", 
          SuperscriptBox["\[Sigma]", "\[Prime]",
           MultilineFunction->None], ")"}], "2"]}]}], 
      SuperscriptBox["\[Sigma]", "2"]], " ", "formula", " ", "found", " ", 
     "by", " ", "hand"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"CFI", "=", 
   RowBox[{"-", 
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"p", "[", 
        RowBox[{"x", ",", "h"}], "]"}], 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"Log", "[", 
          RowBox[{"p", "[", 
           RowBox[{"x", ",", "h"}], "]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"h", ",", "2"}], "}"}]}], "]"}]}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"x", ",", 
        RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
     "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.871041440346611*^9, 3.871041541666524*^9}, {
  3.871043122675686*^9, 3.871043131891529*^9}, {3.8710437911337748`*^9, 
  3.871043797950939*^9}, {3.8710438321158733`*^9, 3.871043856813064*^9}, {
  3.8710446142264233`*^9, 3.8710446143629723`*^9}, {3.8710446928226347`*^9, 
  3.8710446931005363`*^9}, {3.871044825575652*^9, 
  3.871044851196087*^9}},ExpressionUUID->"dcf76896-87b8-4386-84f4-\
2b6a276e6771"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox[
    RowBox[{
     SuperscriptBox["\[Mu]", "\[Prime]",
      MultilineFunction->None], "[", "h", "]"}], "2"], "+", 
   RowBox[{"2", " ", 
    SuperscriptBox[
     RowBox[{
      SuperscriptBox["\[Sigma]", "\[Prime]",
       MultilineFunction->None], "[", "h", "]"}], "2"]}]}], 
  SuperscriptBox[
   RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]]], "Output",
 CellChangeTimes->{3.8710438397298326`*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"b0f0ec38-05cd-430e-8f3a-6a0e1e68e6c8"]
}, Open  ]],

Cell[TextData[{
 "What estimate saturates the CFI? There are a few cases:\n(1) If ",
 Cell[BoxData[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Sigma]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}], "=", "0"}]], "Output",
  GeneratedCell->False,
  CellAutoOverwrite->False,
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "335a2051-dfec-4109-8004-8b9cbcabf648"],
 ", then estimate Y = X such that the signal is  ",
 Cell[BoxData[
  RowBox[{
   SuperscriptBox["\[Mu]", "\[Prime]",
    MultilineFunction->None], "[", "h", "]"}]], "Output",
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "8def374f-f627-46c4-ab77-d4057cf1f4a0"],
 " and the SNR = ",
 Cell[BoxData[
  FractionBox[
   RowBox[{
    SuperscriptBox["\[Mu]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}], 
   RowBox[{"\[Sigma]", "[", "h", "]"}]]], "Output",
  GeneratedCell->False,
  CellAutoOverwrite->False,
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "7fb7653c-3068-4fdf-8dc4-eb2cda5d923d"],
 " saturates the Sqrt[CFI]\n(2) If ",
 Cell[BoxData[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Mu]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}], "=", "0"}]], "Output",
  GeneratedCell->False,
  CellAutoOverwrite->False,
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "b91b0173-def3-4c23-887f-769e16697b19"],
 ", then estimate Y = (X - ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"\[Mu]", ")"}], "2"], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "4c26d0a7-a439-45fa-8db8-a2a0387d9603"],
 " such that the signal is ",
 Cell[BoxData[
  RowBox[{
   RowBox[{
    SubscriptBox["\[PartialD]", "h"], 
    RowBox[{
     SubscriptBox["E", 
      RowBox[{"p", "[", "x", "]"}]], "[", "Y", "]"}]}], "=", 
   RowBox[{"2", 
    RowBox[{
     SuperscriptBox["\[Sigma]", "\[Prime]",
      MultilineFunction->None], "[", "h", "]"}], 
    RowBox[{"\[Sigma]", "[", "h", "]"}]}]}]], "Output",
  GeneratedCell->False,
  CellAutoOverwrite->False,
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "debecc93-5d0a-4fb6-9c0b-c15a7bf221e4"],
 "and the noise is ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{
      SubscriptBox["Var", 
       RowBox[{"p", "[", "X", "]"}]], "[", "Y", "]"}], "]"}], "=", 
    RowBox[{
     RowBox[{"Sqrt", "[", 
      RowBox[{
       RowBox[{
        SubscriptBox["E", 
         RowBox[{"p", "[", "X", "]"}]], "[", 
        RowBox[{"(", 
         RowBox[{"X", "-", 
          FormBox[
           SuperscriptBox[
            RowBox[{"\[Mu]", ")"}], "4"],
           TraditionalForm]}]}], "]"}], "-", 
       SuperscriptBox[
        RowBox[{
         SubscriptBox["E", 
          RowBox[{"p", "[", "X", "]"}]], "[", 
         RowBox[{"(", 
          RowBox[{"X", "-", 
           FormBox[
            SuperscriptBox[
             RowBox[{"\[Mu]", ")"}], "2"],
            TraditionalForm]}]}], "]"}], "2"]}], "]"}], "="}]}], 
   TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "0fed115b-9b60-4a7e-b587-332f99b1d86a"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"Sqrt", "[", "2", "]"}], 
   SuperscriptBox[
    RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]}]], "Output",
  GeneratedCell->False,
  CellAutoOverwrite->False,
  CellChangeTimes->{3.8710438397298326`*^9},ExpressionUUID->
  "d4d067f7-2178-4d8e-81c5-1cfa8dca0fff"],
 ".\n\nFor a general estimator Y = f(X) of h from measuring X with p(x|h), \
the linear SNR = ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{
     SubscriptBox["\[PartialD]", "h"], 
     RowBox[{
      SubscriptBox["E", "p"], "[", "Y", "]"}]}], 
    RowBox[{"Sqrt", "[", 
     RowBox[{
      SubscriptBox["Var", "p"], "[", "Y", "]"}], "]"}]], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "df27c5fc-a72b-482a-b465-c2d2cd4d1bee"],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{
     SubscriptBox["\[PartialD]", "h"], 
     RowBox[{"E", "[", 
      RowBox[{"f", "(", "X", ")"}], "]"}]}], 
    RowBox[{"Sqrt", "[", 
     RowBox[{
      RowBox[{"E", "[", 
       SuperscriptBox[
        RowBox[{"f", "(", "X", ")"}], "2"], "]"}], "-", 
      SuperscriptBox[
       RowBox[{"E", "[", 
        RowBox[{"f", "(", "X", ")"}], "]"}], "2"]}], "]"}]], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "bc4a2266-1c1f-4742-a447-1733b5e29a02"],
 "."
}], "Text",
 CellChangeTimes->{{3.8710446204491777`*^9, 3.871044752491699*^9}, {
  3.871044868393977*^9, 3.8710450504353027`*^9}, {3.8710450976741*^9, 
  3.871045267271366*^9}, {3.871045301452963*^9, 3.871045326026767*^9}, {
  3.871046105282987*^9, 3.871046130750175*^9}, {3.8710605215495768`*^9, 
  3.8710605817834387`*^9}, {3.871060680418013*^9, 3.871060732612829*^9}, {
  3.871060770051498*^9, 
  3.871060905196456*^9}},ExpressionUUID->"3b773c67-0631-40c7-9eb0-\
b6085a05df56"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], 
     SuperscriptBox["z", "2"]}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"z", ",", 
     RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{
    SuperscriptBox["z", "2"], 
    RowBox[{"Exp", "[", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], 
      SuperscriptBox["z", "2"]}], "]"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"z", ",", 
     RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{
    SuperscriptBox["z", "4"], 
    RowBox[{"Exp", "[", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], 
      SuperscriptBox["z", "2"]}], "]"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"z", ",", 
     RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.87105872800117*^9, 3.8710587482616587`*^9}},
 CellLabel->"In[26]:=",ExpressionUUID->"54803776-67c5-4b5a-bbad-c9024b487612"],

Cell[BoxData[
 SqrtBox[
  RowBox[{"2", " ", "\[Pi]"}]]], "Output",
 CellChangeTimes->{{3.871058740158389*^9, 3.8710587492318897`*^9}},
 CellLabel->"Out[26]=",ExpressionUUID->"497d0a79-dce0-459e-bd52-2eadd7d0c7cf"],

Cell[BoxData[
 SqrtBox[
  RowBox[{"2", " ", "\[Pi]"}]]], "Output",
 CellChangeTimes->{{3.871058740158389*^9, 3.871058749379064*^9}},
 CellLabel->"Out[27]=",ExpressionUUID->"600ef027-c48d-45a8-8472-ae2bfe13fa1e"],

Cell[BoxData[
 RowBox[{"3", " ", 
  SqrtBox[
   RowBox[{"2", " ", "\[Pi]"}]]}]], "Output",
 CellChangeTimes->{{3.871058740158389*^9, 3.871058749522151*^9}},
 CellLabel->"Out[28]=",ExpressionUUID->"af36ebc9-b52c-43fa-a6f5-57966f1ae5c3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         SuperscriptBox["x", "2"]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"h", ",", "1"}], "}"}]}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       SuperscriptBox["\[Mu]", "\[Prime]",
        MultilineFunction->None], "[", "h", "]"}], "\[Rule]", "0"}], "}"}]}], 
   
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox["x", "4"]}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         SuperscriptBox["x", "2"]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}]], "Input",
 CellLabel->"In[43]:=",ExpressionUUID->"c5a7c995-4ffb-4423-b488-3154ee3c729e"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   RowBox[{
    SuperscriptBox["\[Sigma]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}]}], 
  SqrtBox[
   RowBox[{
    RowBox[{"2", " ", 
     SuperscriptBox[
      RowBox[{"\[Mu]", "[", "h", "]"}], "2"]}], "+", 
    SuperscriptBox[
     RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]}]]]], "Output",
 CellChangeTimes->{3.871059350649214*^9},
 CellLabel->"Out[43]=",ExpressionUUID->"e3203e6f-b759-4891-aac7-d9042aba05e7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["x", "2"], "-", "b"}], ")"}]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"h", ",", "1"}], "}"}]}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       SuperscriptBox["\[Mu]", "\[Prime]",
        MultilineFunction->None], "[", "h", "]"}], "\[Rule]", "0"}], "}"}]}], 
   
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["x", "2"], "-", "b"}], ")"}], "2"]}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["x", "2"], "-", "b"}], ")"}]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.87105931987661*^9, 3.871059368841139*^9}},
 CellLabel->"In[44]:=",ExpressionUUID->"8ce72c3f-4f34-4ccf-a750-9e991b0d378b"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   RowBox[{
    SuperscriptBox["\[Sigma]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}]}], 
  SqrtBox[
   RowBox[{
    RowBox[{"2", " ", 
     SuperscriptBox[
      RowBox[{"\[Mu]", "[", "h", "]"}], "2"]}], "+", 
    SuperscriptBox[
     RowBox[{"\[Sigma]", "[", "h", "]"}], "2"]}]]]], "Output",
 CellChangeTimes->{{3.871059348962267*^9, 3.871059372914217*^9}},
 CellLabel->"Out[44]=",ExpressionUUID->"616162b5-b8d9-4de1-9429-bed43846d507"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", "b"}], ")"}], "2"], ")"}]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"h", ",", "1"}], "}"}]}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       SuperscriptBox["\[Mu]", "\[Prime]",
        MultilineFunction->None], "[", "h", "]"}], "\[Rule]", "0"}], "}"}]}], 
   
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox[
         RowBox[{"(", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", "b"}], ")"}], "2"], ")"}], "2"]}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", "b"}], ")"}], "2"], ")"}]}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.871059417510005*^9, 3.871059441114262*^9}},
 CellLabel->"In[45]:=",ExpressionUUID->"6fae6e39-1252-4f13-9dbe-6b446c14b5a6"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[Sigma]", "\[Prime]",
    MultilineFunction->None], "[", "h", "]"}], 
  SqrtBox[
   RowBox[{
    SuperscriptBox["b", "2"], "-", 
    RowBox[{"2", " ", "b", " ", 
     RowBox[{"\[Mu]", "[", "h", "]"}]}], "+", 
    SuperscriptBox[
     RowBox[{"\[Mu]", "[", "h", "]"}], "2"], "+", 
    FractionBox[
     SuperscriptBox[
      RowBox[{"\[Sigma]", "[", "h", "]"}], "2"], "2"]}]]]], "Output",
 CellChangeTimes->{3.8710594496896677`*^9},
 CellLabel->"Out[45]=",ExpressionUUID->"aee1a252-0571-4ae2-ace6-412195512c9a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  FractionBox[
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], "x"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
      "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"h", ",", "1"}], "}"}]}], "]"}], 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox[
         RowBox[{"(", "x", ")"}], "2"]}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], "x"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{
  FractionBox[
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        RowBox[{"(", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x", "-", 
            RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"], ")"}]}], ",", 
       " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
      "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"h", ",", "1"}], "}"}]}], "]"}], 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox[
         RowBox[{"(", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", 
             RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"], ")"}], "2"]}], 
       ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", 
             RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"], ")"}]}], ",", 
        " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}]}], "Input",
 CellChangeTimes->{{3.8710602953346653`*^9, 3.871060309690887*^9}, {
  3.87106036628941*^9, 3.871060377152594*^9}},
 CellLabel->"In[47]:=",ExpressionUUID->"f44b3ece-c7c6-4631-9bf9-c045da2932c2"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[Mu]", "\[Prime]",
    MultilineFunction->None], "[", "h", "]"}], 
  RowBox[{"\[Sigma]", "[", "h", "]"}]]], "Output",
 CellChangeTimes->{3.8710603141318483`*^9, 3.8710603792093487`*^9},
 CellLabel->"Out[47]=",ExpressionUUID->"90c4fa97-b7bc-4673-bf50-75b1eede6541"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   RowBox[{
    SuperscriptBox["\[Sigma]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}]}], 
  RowBox[{"\[Sigma]", "[", "h", "]"}]]], "Output",
 CellChangeTimes->{3.8710603141318483`*^9, 3.87106038148393*^9},
 CellLabel->"Out[48]=",ExpressionUUID->"2e8e4426-088f-4aad-bd05-8f952e1dad58"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        RowBox[{"(", 
         RowBox[{"x", "+", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"x", "-", 
             RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"]}], ")"}]}], ",", 
       " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], " ", 
      "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"h", ",", "1"}], "}"}]}], "]"}], 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"p", "[", 
         RowBox[{"x", ",", "h"}], "]"}], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"x", "+", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"x", "-", 
              RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"]}], ")"}], 
         "2"]}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", 
         RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
     "-", 
     SuperscriptBox[
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p", "[", 
          RowBox[{"x", ",", "h"}], "]"}], 
         RowBox[{"(", 
          RowBox[{"x", "+", 
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"x", "-", 
              RowBox[{"\[Mu]", "[", "h", "]"}]}], ")"}], "2"]}], ")"}]}], ",",
         " ", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
      "2"]}], "]"}]], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.871060389373227*^9, 3.871060394444831*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"acf54000-bfed-4c27-83d1-e89309a365e7"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Mu]", "\[Prime]",
     MultilineFunction->None], "[", "h", "]"}], "+", 
   RowBox[{"2", " ", 
    RowBox[{"\[Sigma]", "[", "h", "]"}], " ", 
    RowBox[{
     SuperscriptBox["\[Sigma]", "\[Prime]",
      MultilineFunction->None], "[", "h", "]"}]}]}], 
  SqrtBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"\[Sigma]", "[", "h", "]"}], "2"], "+", 
    RowBox[{"2", " ", 
     SuperscriptBox[
      RowBox[{"\[Sigma]", "[", "h", "]"}], "4"]}]}]]]], "Output",
 CellChangeTimes->{3.8710603976987886`*^9},
 CellLabel->"Out[49]=",ExpressionUUID->"96f85941-7492-45cd-9370-44a6d621b76a"]
}, Open  ]],

Cell["Now for displacement operators", "Text",
 CellFrame->{{0, 0}, {0, 3}},
 CellChangeTimes->{{3.872167547391054*^9, 
  3.872167555176446*^9}},ExpressionUUID->"e1b4235a-2af1-4bbf-a7b8-\
4dda8cbe6360"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"\[Theta]1", "\[Element]", "Reals"}], ",", 
     RowBox[{"\[Theta]2", "\[Element]", "Reals"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"p", "[", "x_", "]"}], ":=", 
  RowBox[{
   FractionBox["1", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"2", "\[Pi]"}], "]"}]], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"x", "-", 
        RowBox[{"Sqrt", "[", 
         FractionBox[
          RowBox[{
           SuperscriptBox["\[Theta]1", "2"], "+", 
           SuperscriptBox["\[Theta]2", "2"]}], "2"], "]"}]}], ")"}], "2"]}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CFI", "=", 
   RowBox[{"(", GridBox[{
      {
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"p", "[", "x", "]"}], 
          SuperscriptBox[
           RowBox[{"D", "[", 
            RowBox[{
             RowBox[{"Log", "[", 
              RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"\[Theta]1", ",", "1"}], "}"}]}], "]"}], "2"]}], ",", 
         " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", 
           RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"p", "[", "x", "]"}], 
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"Log", "[", 
             RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Theta]1", ",", "1"}], "}"}]}], "]"}], 
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"Log", "[", 
             RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Theta]2", ",", "1"}], "}"}]}], "]"}]}], ",", " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", 
           RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}]},
      {
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"p", "[", "x", "]"}], 
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"Log", "[", 
             RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Theta]1", ",", "1"}], "}"}]}], "]"}], 
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"Log", "[", 
             RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Theta]2", ",", "1"}], "}"}]}], "]"}]}], ",", " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", 
           RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "]"}], 
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"p", "[", "x", "]"}], 
          SuperscriptBox[
           RowBox[{"D", "[", 
            RowBox[{
             RowBox[{"Log", "[", 
              RowBox[{"p", "[", "x", "]"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"\[Theta]2", ",", "1"}], "}"}]}], "]"}], "2"]}], ",", 
         " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", 
           RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
        "]"}]}
     }], ")"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"CFI", "//", "MatrixForm"}], "]"}]}], "Input",
 CellChangeTimes->{{3.8721676175756073`*^9, 3.872167868060198*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"f41a6135-65d1-4a6f-8d48-8d9afb70092c"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      FractionBox[
       SuperscriptBox["\[Theta]1", "2"], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["\[Theta]1", "2"], "+", 
          SuperscriptBox["\[Theta]2", "2"]}], ")"}]}]], 
      FractionBox[
       RowBox[{"\[Theta]1", " ", "\[Theta]2"}], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["\[Theta]1", "2"], "+", 
          SuperscriptBox["\[Theta]2", "2"]}], ")"}]}]]},
     {
      FractionBox[
       RowBox[{"\[Theta]1", " ", "\[Theta]2"}], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["\[Theta]1", "2"], "+", 
          SuperscriptBox["\[Theta]2", "2"]}], ")"}]}]], 
      FractionBox[
       SuperscriptBox["\[Theta]2", "2"], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["\[Theta]1", "2"], "+", 
          SuperscriptBox["\[Theta]2", "2"]}], ")"}]}]]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Print",
 CellChangeTimes->{3.872167876258191*^9},
 CellLabel->
  "During evaluation of \
In[5]:=",ExpressionUUID->"c5654524-7907-4d0d-a15f-41e391482480"]
}, Open  ]]
},
WindowSize->{1853, 1025},
WindowMargins->{{0, Automatic}, {0, Automatic}},
Magnification:>1.5 Inherited,
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 741, 14, 178, "Text",ExpressionUUID->"c30710f4-55d3-4376-acb1-16b3f2d1ede2"],
Cell[1302, 36, 1221, 38, 157, "Input",ExpressionUUID->"e1a8ec7b-781e-42c5-8e89-382851d8ca88"],
Cell[CellGroupData[{
Cell[2548, 78, 763, 25, 116, "Input",ExpressionUUID->"86f81243-c648-42ba-b779-74f69b36d527"],
Cell[3314, 105, 307, 8, 85, "Output",ExpressionUUID->"6864a8e0-44fe-4b16-9a4b-877775715091"],
Cell[3624, 115, 939, 28, 86, "Output",ExpressionUUID->"0f2a57e7-dde7-420a-b496-d5d5641e0e9a"],
Cell[4566, 145, 2296, 68, 86, "Output",ExpressionUUID->"1898a327-af66-48d4-836e-c3540ce23d13"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6899, 218, 1623, 43, 110, "Input",ExpressionUUID->"dcf76896-87b8-4386-84f4-2b6a276e6771"],
Cell[8525, 263, 532, 15, 91, "Output",ExpressionUUID->"b0f0ec38-05cd-430e-8f3a-6a0e1e68e6c8"]
}, Open  ]],
Cell[9072, 281, 4882, 146, 276, "Text",ExpressionUUID->"3b773c67-0631-40c7-9eb0-b6085a05df56"],
Cell[CellGroupData[{
Cell[13979, 431, 1201, 39, 226, "Input",ExpressionUUID->"54803776-67c5-4b5a-bbad-c9024b487612"],
Cell[15183, 472, 213, 4, 56, "Output",ExpressionUUID->"497d0a79-dce0-459e-bd52-2eadd7d0c7cf"],
Cell[15399, 478, 211, 4, 56, "Output",ExpressionUUID->"600ef027-c48d-45a8-8472-ae2bfe13fa1e"],
Cell[15613, 484, 235, 5, 56, "Output",ExpressionUUID->"af36ebc9-b52c-43fa-a6f5-57966f1ae5c3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15885, 494, 1487, 47, 99, "Input",ExpressionUUID->"c5a7c995-4ffb-4423-b488-3154ee3c729e"],
Cell[17375, 543, 492, 15, 108, "Output",ExpressionUUID->"e3203e6f-b759-4891-aac7-d9042aba05e7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17904, 563, 1772, 55, 99, "Input",ExpressionUUID->"8ce72c3f-4f34-4ccf-a750-9e991b0d378b"],
Cell[19679, 620, 516, 15, 108, "Output",ExpressionUUID->"616162b5-b8d9-4de1-9429-bed43846d507"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20232, 640, 1872, 58, 99, "Input",ExpressionUUID->"6fae6e39-1252-4f13-9dbe-6b446c14b5a6"],
Cell[22107, 700, 569, 16, 127, "Output",ExpressionUUID->"aee1a252-0571-4ae2-ace6-412195512c9a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22713, 721, 3042, 96, 189, "Input",ExpressionUUID->"f44b3ece-c7c6-4631-9bf9-c045da2932c2"],
Cell[25758, 819, 323, 7, 84, "Output",ExpressionUUID->"90c4fa97-b7bc-4673-bf50-75b1eede6541"],
Cell[26084, 828, 362, 9, 91, "Output",ExpressionUUID->"2e8e4426-088f-4aad-bd05-8f952e1dad58"]
}, Open  ]],
Cell[CellGroupData[{
Cell[26483, 842, 1923, 60, 89, "Input",ExpressionUUID->"acf54000-bfed-4c27-83d1-e89309a365e7"],
Cell[28409, 904, 654, 19, 101, "Output",ExpressionUUID->"96f85941-7492-45cd-9370-44a6d621b76a"]
}, Open  ]],
Cell[29078, 926, 202, 4, 71, "Text",ExpressionUUID->"e1b4235a-2af1-4bbf-a7b8-4dda8cbe6360"],
Cell[CellGroupData[{
Cell[29305, 934, 3612, 105, 355, "Input",ExpressionUUID->"f41a6135-65d1-4a6f-8d48-8d9afb70092c"],
Cell[32920, 1041, 1660, 49, 127, "Print",ExpressionUUID->"c5654524-7907-4d0d-a15f-41e391482480"]
}, Open  ]]
}
]
*)

